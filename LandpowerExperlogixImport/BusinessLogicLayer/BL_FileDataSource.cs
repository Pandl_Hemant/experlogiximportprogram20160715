﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using DataAccessLayer;
using System.Data;

namespace BusinessLogicLayer
{
    public class BL_FileDataSource
    {
        string _fileName = string.Empty;
        string _filePath = string.Empty;
        DL_FileDataSource _dl_fileDataSource = null;

        public BL_FileDataSource(string fileName, string filePath)
        {
            this._fileName = fileName;
            this._filePath = filePath;
            this._dl_fileDataSource = new DL_FileDataSource();
        }

        public List<string> GetFileSheetNameList()
        {
            return this._dl_fileDataSource.GetFileSheetNameList(this._filePath);
        }

        public DataTable GetFileSheetData(string sheetName)
        {
            return this._dl_fileDataSource.GetFileSheetData(sheetName, this._filePath);
        }

        public string FileName
        {
            get
            {
                return this._fileName;
            }
            set
            {
                this._fileName = value;
            }
        }

        public string FilePath
        {
            get
            {
                return this._filePath;
            }
            set
            {
                this._filePath = value;
            }
        }

    }
}
