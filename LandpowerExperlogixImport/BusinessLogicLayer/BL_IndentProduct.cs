﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using DataAccessLayer;
using System.Windows.Forms;
using System.Data;

namespace BusinessLogicLayer
{
    public class BL_IndentProduct
    {
        
        DL_IndentProduct _DL_indentProduct = null;
        string _importFilename = string.Empty; // name of the import file
        int _processID = 0; // next process ID to be used for logging
        string _selectedDatabaseName = string.Empty;
        bool _isSelectedDatabaseChanged = true; // true query to check. 
        bool _isSelectedDatabaseValid = false; // whether database selected is valid for indent import
        string _type = "Indent";  // filter to query whether database selected is valid

        public BL_IndentProduct(string importFilename, string selectedDatabaseName)
        {
            this._DL_indentProduct = new DL_IndentProduct();
            this._importFilename = importFilename;
            this._selectedDatabaseName = selectedDatabaseName;
  
        }

        public BL_IndentProduct(string selectedDatabaseName)
        {
            this._DL_indentProduct = new DL_IndentProduct();
            this._selectedDatabaseName = selectedDatabaseName;
        }

        /// <summary>
        /// Load next process ID
        /// </summary>
        private void LoadProcessID()
        {
            this._processID = this._DL_indentProduct.LoadNextProcessID();
        }

        public void ImportNewModel(DataGridView dv, string mode, string databaseName)
        {

            // step 1. Clear template tables first
            ClearSQLTempTables();

            // step 2. Import all data to template table
            ImportDataToSQLTemplate(dv);

            // step 3. Loop thru each product to import into Experlogix
            ImportEachNewModel(mode, databaseName);

        }

        /// <summary>
        /// Clear all SQL temp tables before import data. 
        /// </summary>
        private void ClearSQLTempTables()
        {
            this._DL_indentProduct.ClearIndentSQLTemplateTable();
        }

        /// <summary>
        /// Import all data loaded on screen to sql template table.
        /// </summary>
        private void ImportDataToSQLTemplate(DataGridView dv)
        {
            this._DL_indentProduct.ImportDataToSQLTemplateTable(dv);
        }

        /// <summary>
        /// Loop thru all products and import each model. Commmit any model that can be completed and flag models that are errored.
        /// </summary>
        /// <param name="mode"></param>
        private void ImportEachNewModel(string mode, string databaseName)
        {
            // get new process ID for each import
             LoadProcessID();
            this._DL_indentProduct.ImportEachNewModel(this._importFilename, this._processID, mode, databaseName);
        }

        /// <summary>
        /// Return messages logged from import. Return based on the max ID.  
        /// </summary>
        /// <returns></returns>
        public List<String> GetMessageLogged()
        {
            List<String> messageList = this._DL_indentProduct.GetMessageLogged();

            return messageList;
        }


        /// <summary>
        /// Get error data and export to error folder
        /// </summary>
        public void ExportErrorDataToFile()
        {
            this._DL_indentProduct.ExportErrorDataToFile();
        }

        /// <summary>
        /// Get processed data and export to complete folder
        /// </summary>
        public void ExportCompletedDataToFile()
        {
            this._DL_indentProduct.ExportCompletedDataToFile();
        }

        /// <summary>
        /// Query database to check and see if it is valid.
        /// </summary>
        /// <returns></returns>
        private bool LoadIsDatabaseSelectedIsValid()
        {
            return this._DL_indentProduct.LoadIsDatabaseSelectedIsValid(this._selectedDatabaseName,this._type);
        }

        public string SelectedDatabaseName{
            get
            {
                return this._selectedDatabaseName;
            }
            set
            {
                this._selectedDatabaseName = value;
                this._isSelectedDatabaseChanged = true;
            }
        }

        public bool IsSelectedDatabaseNameValid
        {
            get
            {
                if (_isSelectedDatabaseChanged)
                {
                    // re-check status if database name changed
                    this._isSelectedDatabaseValid = this.LoadIsDatabaseSelectedIsValid();
                    this._isSelectedDatabaseChanged = false;
                }
                return this._isSelectedDatabaseValid;
            }
        }

        public string ImportFileName
        {
            get
            {
                return this._importFilename;
            }
            set
            {
                this._importFilename = value;
            }
        }

        public  DataTable GetOptionCategoryAssignment(string optionID)
        {
            return this._DL_indentProduct.GetOptionCategoryAssignment(optionID, this._selectedDatabaseName);
        }

        public DataTable GetOptionModelAssignment(string optionID)
        {
            return this._DL_indentProduct.GetOoptionModelAssignment(optionID,this._selectedDatabaseName);
        }

        public DataTable GetOptionPrice(string optionID)
        {
            return this._DL_indentProduct.GetOoptionPrice(optionID,this._selectedDatabaseName);
        }

        public DataTable GetOptionModelRuleAssignment(string optionID)
        {
            return this._DL_indentProduct.GetOptionModelRuleAssignment(optionID,this._selectedDatabaseName);
        }

        public DataTable GetOptionProperty(string optionID)
        {
            return this._DL_indentProduct.GetOptionProperty(optionID, this._selectedDatabaseName);
        }

        public string DeleteOption(string optionID)
        {
            return this._DL_indentProduct.DeleteOption(optionID, this._selectedDatabaseName);
        }

        public void EmailImportComplete(string emailbody)
        {
            this._DL_indentProduct.EmailImportComplete(emailbody);
        }

    }
}
