﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using DataAccessLayer;
using System.Windows.Forms;
using System.Data;


namespace BusinessLogicLayer
{
    public class BL_Instock
    {
        DL_Instock _DL_Instock = null;
        string _importFilename = string.Empty; // name of the import file
        int _processID = 0; // next process ID to be used for logging
        string _selectedDatabaseName = string.Empty;
        bool _isSelectedDatabaseChanged = true; // true query to check. 
        bool _isSelectedDatabaseValid = false; // whether database selected is valid for indent import
        string _type = "Instock"; // filter to query whether database selected is valid

         public BL_Instock(string importFilename, string selectedDatabaseName)
        {
            this._DL_Instock = new DL_Instock();
            this._importFilename = importFilename;
            this._selectedDatabaseName = selectedDatabaseName;  
        }

         public BL_Instock(string selectedDatabaseName)
        {
            this._DL_Instock = new DL_Instock();
            this._selectedDatabaseName = selectedDatabaseName;

        }

         public void ImportNewAndUpdateInstock(DataGridView dv, string databaseName)
         {

             // step 1. Clear template tables first
             ClearSQLTempTables();

             // step 2. Import all data to template table
             ImportDataToSQLTemplate(dv);

             // step 3. Loop thru each product to import into Experlogix
             ImportAndUpdateEachInstock(databaseName);

         }

         public void ImportNewAndUpdateInstock(DataTable dtSheetData, string databaseName)
         {

             // step 1. Clear template tables first
             ClearSQLTempTables();

             // step 2. Import all data to template table
             ImportDataToSQLTemplate(dtSheetData);

             // step 3. Loop thru each product to import into Experlogix
             ImportAndUpdateEachInstock(databaseName);

         }

         /// <summary>
         /// Clear all SQL temp tables before import data. 
         /// </summary>
         private void ClearSQLTempTables()
         {
             this._DL_Instock.ClearInstockSQLTemplateTable();
         }

         /// <summary>
         /// Import all data loaded on screen to sql template table.
         /// </summary>
         private void ImportDataToSQLTemplate(DataGridView dv)
         {
             this._DL_Instock.ImportDataToSQLTemplateTable(dv);
         }

         private void ImportDataToSQLTemplate(DataTable dtSheetData)
         {
             this._DL_Instock.ImportDataToSQLTemplateTable(dtSheetData);
         }

         /// <summary>
         /// Loop thru all products and import each model. Commmit any model that can be completed and flag models that are errored.
         /// </summary>
         /// <param name="mode"></param>
         private void ImportAndUpdateEachInstock(string databaseName)
         {
             // get new process ID for each import
             LoadProcessID();
             this._DL_Instock.ImportAndUpdateEachInstock(this._importFilename, this._processID,databaseName);

         }


         /// <summary>
         /// Return messages logged from import. Return based on the max ID.  
         /// </summary>
         /// <returns></returns>
         public List<String> GetMessageLogged()
         {
             List<String> messageList = this._DL_Instock.GetMessageLogged();

             return messageList;
         }


         /// <summary>
         /// Get error data and export to error folder
         /// </summary>
         public void ExportErrorDataToFile()
         {
             this._DL_Instock.ExportErrorDataToFile();
         }

         /// <summary>
         /// Get processed data and export to complete folder
         /// </summary>
         public void ExportCompletedDataToFile()
         {
             this._DL_Instock.ExportCompletedDataToFile();
         }
         

         /// <summary>
         /// Load next process ID
         /// </summary>
         private void LoadProcessID()
         {
             this._processID = this._DL_Instock.LoadNextProcessID();
         }

         /// <summary>
         /// Query database to check and see if it is valid.
         /// </summary>
         /// <returns></returns>
         private bool LoadIsDatabaseSelectedIsValid()
         {
             return this._DL_Instock.LoadIsDatabaseSelectedIsValid(this._selectedDatabaseName, this._type);
         }

         public string SelectedDatabaseName
         {
             get
             {
                 return this._selectedDatabaseName;
             }
             set
             {
                 this._selectedDatabaseName = value;
                 this._isSelectedDatabaseChanged = true;
             }
         }

         public bool IsSelectedDatabaseNameValid
         {
             get
             {
                 if (_isSelectedDatabaseChanged)
                 {
                     // re-check status if database name changed
                     this._isSelectedDatabaseValid = this.LoadIsDatabaseSelectedIsValid();
                     this._isSelectedDatabaseChanged = false;
                 }
                 return this._isSelectedDatabaseValid;
             }
         }

         public void EmailImportComplete(string emailbody)
         {
             this._DL_Instock.EmailImportComplete(emailbody);
         }


         public string ImportFilename
         {
             get
             {
                 return this._importFilename;
             }
             set
             {
                 this._importFilename = value;
             }
         }
    }
}
