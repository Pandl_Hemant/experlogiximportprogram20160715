﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public class BL_ExperlogixDatabase
    {

        #region variables

        //private string _className = "[BL_ExperlogixDatabase]";

        private DL_ExperlogixDatabase _dataAccess; 

        #endregion


        /// <summary>
        /// Default construtor
        /// </summary>
        public BL_ExperlogixDatabase()
        {
            this._dataAccess = new DL_ExperlogixDatabase();
        }

        /// <summary>
        /// Get all available Experlogix databases.
        /// </summary>
        /// <returns>Return a list of all Experlogix database.</returns>
        public List<string> GetExperlogixDatabaseList()
        {
            return this._dataAccess.GetExperlogixDatabaseList();
        }


    }
}
