﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Common;
using System.Windows.Forms;
using System.Transactions;
using RKLib.ExportData;
using System.Net.Mail;

namespace DataAccessLayer
{
    public class DL_Instock
    {
        private string className = "[DL_Instock]";

        public DL_Instock()
        {

        }

        public int LoadNextProcessID()
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);
            int nextProcessID = 1;

            try
            {
                string sql = "IMPORT_GetNextProcessID";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // add arguements

                SqlDataReader myReader = null;
                myReader = sqlcmd.ExecuteReader();

                while (myReader.Read())
                {
                    nextProcessID = (int)myReader["NextProcessID"];
                }

                sqlcmd.Parameters.Clear();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[LoadNextProcessID]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            return nextProcessID;
        } // end method

        public bool LoadIsDatabaseSelectedIsValid(string selectedDatabaseName, string type)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            bool isDatabaseNameSelectedValid = false;

            try
            {
                string sql = "IMPORT_IsDatabaseSelectedValid";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@DatabaseName", selectedDatabaseName);
                sqlcmd.Parameters.AddWithValue("@Type", type);

                SqlDataReader myReader = null;
                myReader = sqlcmd.ExecuteReader();

                while (myReader.Read())
                {
                    isDatabaseNameSelectedValid = Convert.ToBoolean(myReader["IsValid"]);
                }

                sqlcmd.Parameters.Clear();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[LoadIsDatabaseSelectedIsValid]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            return isDatabaseNameSelectedValid;
        } // end  method

        /// <summary>
        /// Clear Indent import tables
        /// </summary>
        public void ClearInstockSQLTemplateTable()
        {

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            try
            {
                string sql = "IMPORT_Instock_ResetTemplateInstock1";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ClearInstockSQLTemplateTable]") + ex.Message);
            }
            finally
            {
                con.Close();
            }
        } // end method

        public void ImportDataToSQLTemplateTable(DataGridView dv)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            string sql = "IMPORT_Instock_ImportGridDataToTempTable";

            SqlCommand sqlcmd = new SqlCommand(sql, con);
            con.Open();

            sqlcmd.CommandType = CommandType.StoredProcedure;

            SqlTransaction transaction;
            // Start a local transaction.
            transaction = con.BeginTransaction("InsertDataToTemp");
            sqlcmd.Transaction = transaction;
            bool allInsertSuccessful = true;
            int recordInserted = 0;

            try
            {

                Console.WriteLine("row count = " + dv.Rows.Count.ToString());

                foreach (DataGridViewRow row in dv.Rows)
                {
                    // Last row is causing object reference issue
                    // +1 to advance to next record
                    if (recordInserted + 1 == dv.Rows.Count)
                    {
                        break;
                    }

                    sqlcmd.Parameters.AddWithValue("@Image", row.Cells[(int)ImportTemp1TableColumnName_Instock.Image].Value);
                    sqlcmd.Parameters.AddWithValue("@ProductCountryAvailability", row.Cells[(int)ImportTemp1TableColumnName_Instock.ProductCountryAvailability].Value);
                    sqlcmd.Parameters.AddWithValue("@SpecType", row.Cells[(int)ImportTemp1TableColumnName_Instock.SpecType].Value);
                    sqlcmd.Parameters.AddWithValue("@row_id", row.Cells[(int)ImportTemp1TableColumnName_Instock.row_id].Value);
                    sqlcmd.Parameters.AddWithValue("@serial_no", row.Cells[(int)ImportTemp1TableColumnName_Instock.serial_no].Value);
                    sqlcmd.Parameters.AddWithValue("@provisional_serial_no", row.Cells[(int)ImportTemp1TableColumnName_Instock.provisional_serial_no].Value);
                    sqlcmd.Parameters.AddWithValue("@entity", row.Cells[(int)ImportTemp1TableColumnName_Instock.entity].Value);
                    sqlcmd.Parameters.AddWithValue("@location", row.Cells[(int)ImportTemp1TableColumnName_Instock.location].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_stock_code", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_stock_code].Value);
                    sqlcmd.Parameters.AddWithValue("@mod_sup_stock_code", row.Cells[(int)ImportTemp1TableColumnName_Instock.mod_sup_stock_code].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_seq_no", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_seq_no].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_type", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_type].Value);
                    sqlcmd.Parameters.AddWithValue("@spec_stock_code", row.Cells[(int)ImportTemp1TableColumnName_Instock.spec_stock_code].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_description", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_description].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_standard_flag", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_standard_flag].Value);
                    sqlcmd.Parameters.AddWithValue("@spec_fitted_flag", row.Cells[(int)ImportTemp1TableColumnName_Instock.spec_fitted_flag].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_qty", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_qty].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_retail_price_aus", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_retail_price_aus].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_retail_price_nz", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_retail_price_nz].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_link", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_link].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_price_list", row.Cells[(int)ImportTemp1TableColumnName_Instock.specs_sales_price_list].Value);
                    sqlcmd.Parameters.AddWithValue("@mach_status", row.Cells[(int)ImportTemp1TableColumnName_Instock.mach_status].Value);
                    sqlcmd.Parameters.AddWithValue("@exp_arrival_date", row.Cells[(int)ImportTemp1TableColumnName_Instock.exp_arrival_date].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_group", row.Cells[(int)ImportTemp1TableColumnName_Instock.prod_group].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_group_desc", row.Cells[(int)ImportTemp1TableColumnName_Instock.prod_group_desc].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_class", row.Cells[(int)ImportTemp1TableColumnName_Instock.prod_class].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_class_desc", row.Cells[(int)ImportTemp1TableColumnName_Instock.prod_class_desc].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_cat", row.Cells[(int)ImportTemp1TableColumnName_Instock.prod_cat].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_cat_desc", row.Cells[(int)ImportTemp1TableColumnName_Instock.prod_cat_desc].Value);
                    sqlcmd.Parameters.AddWithValue("@mach_total_cost", row.Cells[(int)ImportTemp1TableColumnName_Instock.mach_total_cost].Value); // added
                    sqlcmd.Parameters.AddWithValue("@instock_country",row.Cells[(int)ImportTemp1TableColumnName_Instock.instock_country].Value); // added
                    sqlcmd.Parameters.AddWithValue("@AU_series_id", row.Cells[(int)ImportTemp1TableColumnName_Instock.AU_series_id].Value);
                    sqlcmd.Parameters.AddWithValue("@NZ_series_id", row.Cells[(int)ImportTemp1TableColumnName_Instock.NZ_series_id].Value);
                    sqlcmd.Parameters.AddWithValue("@AU_model_id", row.Cells[(int)ImportTemp1TableColumnName_Instock.AU_model_id].Value);
                    sqlcmd.Parameters.AddWithValue("@NZ_model_id", row.Cells[(int)ImportTemp1TableColumnName_Instock.NZ_model_id].Value);                    
                    sqlcmd.Parameters.AddWithValue("@BUID", row.Cells[(int)ImportTemp1TableColumnName_Instock.BUID].Value);
                    sqlcmd.Parameters.AddWithValue("@total_retail_price_au", row.Cells[(int)ImportTemp1TableColumnName_Instock.total_retail_price_au].Value);
                    sqlcmd.Parameters.AddWithValue("@total_retail_price_nz", row.Cells[(int)ImportTemp1TableColumnName_Instock.total_retail_price_nz].Value);




                    sqlcmd.ExecuteNonQuery();

                    sqlcmd.Parameters.Clear();

                    recordInserted++;

                    Console.WriteLine("row id" + row.Cells[(int)ImportTemp1TableColumnName_Instock.row_id].Value.ToString());
                    Console.WriteLine("recordInserted = " + recordInserted.ToString());
                }
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                allInsertSuccessful = false;
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ImportDataToSQLTemplateTable]") + ex.Message);
            }
            finally
            {
                if (allInsertSuccessful == true && recordInserted > 0)
                {
                    transaction.Commit();
                }

                sqlcmd.Dispose();
                con.Close();
            }// end try
        } // end method

        public void ImportDataToSQLTemplateTable(DataTable dtSheetData)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            string sql = "IMPORT_Instock_ImportGridDataToTempTable";

            SqlCommand sqlcmd = new SqlCommand(sql, con);
            con.Open();

            sqlcmd.CommandType = CommandType.StoredProcedure;

            SqlTransaction transaction;
            // Start a local transaction.
            transaction = con.BeginTransaction("InsertDataToTemp");
            sqlcmd.Transaction = transaction;
            bool allInsertSuccessful = true;
            int recordInserted = 0;

            try
            {
                foreach (DataRow row in dtSheetData.Rows)
                {
                    // 2014-05-16 fix: The source is DataTable there won't be extra row so we don't need this check else last row will always get missed out. 
                    // Last row is causing object reference issue
                    // +1 to advance to next record
                    /*if (recordInserted + 1 == dtSheetData.Rows.Count)
                    {
                        break;
                    }*/

                    sqlcmd.Parameters.AddWithValue("@Image", row[(int)ImportTemp1TableColumnName_Instock.Image]);
                    sqlcmd.Parameters.AddWithValue("@ProductCountryAvailability", row[(int)ImportTemp1TableColumnName_Instock.ProductCountryAvailability]);
                    sqlcmd.Parameters.AddWithValue("@SpecType", row[(int)ImportTemp1TableColumnName_Instock.SpecType]);
                    sqlcmd.Parameters.AddWithValue("@row_id", row[(int)ImportTemp1TableColumnName_Instock.row_id]);
                    sqlcmd.Parameters.AddWithValue("@serial_no", row[(int)ImportTemp1TableColumnName_Instock.serial_no]);
                    sqlcmd.Parameters.AddWithValue("@provisional_serial_no", row[(int)ImportTemp1TableColumnName_Instock.provisional_serial_no]);
                    sqlcmd.Parameters.AddWithValue("@entity", row[(int)ImportTemp1TableColumnName_Instock.entity]);
                    sqlcmd.Parameters.AddWithValue("@location", row[(int)ImportTemp1TableColumnName_Instock.location]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_stock_code", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_stock_code]);
                    sqlcmd.Parameters.AddWithValue("@mod_sup_stock_code", row[(int)ImportTemp1TableColumnName_Instock.mod_sup_stock_code]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_seq_no", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_seq_no]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_type", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_type]);
                    sqlcmd.Parameters.AddWithValue("@spec_stock_code", row[(int)ImportTemp1TableColumnName_Instock.spec_stock_code]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_description", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_description]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_standard_flag", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_standard_flag]);
                    sqlcmd.Parameters.AddWithValue("@spec_fitted_flag", row[(int)ImportTemp1TableColumnName_Instock.spec_fitted_flag]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_qty", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_qty]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_retail_price_aus", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_retail_price_aus]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_retail_price_nz", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_retail_price_nz]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_link", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_link]);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_price_list", row[(int)ImportTemp1TableColumnName_Instock.specs_sales_price_list]);
                    sqlcmd.Parameters.AddWithValue("@mach_status", row[(int)ImportTemp1TableColumnName_Instock.mach_status]);
                    sqlcmd.Parameters.AddWithValue("@exp_arrival_date", row[(int)ImportTemp1TableColumnName_Instock.exp_arrival_date]);
                    sqlcmd.Parameters.AddWithValue("@prod_group", row[(int)ImportTemp1TableColumnName_Instock.prod_group]);
                    sqlcmd.Parameters.AddWithValue("@prod_group_desc", row[(int)ImportTemp1TableColumnName_Instock.prod_group_desc]);
                    sqlcmd.Parameters.AddWithValue("@prod_class", row[(int)ImportTemp1TableColumnName_Instock.prod_class]);
                    sqlcmd.Parameters.AddWithValue("@prod_class_desc", row[(int)ImportTemp1TableColumnName_Instock.prod_class_desc]);
                    sqlcmd.Parameters.AddWithValue("@prod_cat", row[(int)ImportTemp1TableColumnName_Instock.prod_cat]);
                    sqlcmd.Parameters.AddWithValue("@prod_cat_desc", row[(int)ImportTemp1TableColumnName_Instock.prod_cat_desc]);
                    sqlcmd.Parameters.AddWithValue("@mach_total_cost", row[(int)ImportTemp1TableColumnName_Instock.mach_total_cost]); // added
                    sqlcmd.Parameters.AddWithValue("@instock_country", row[(int)ImportTemp1TableColumnName_Instock.instock_country]); // added
                    sqlcmd.Parameters.AddWithValue("@AU_series_id", row[(int)ImportTemp1TableColumnName_Instock.AU_series_id]);
                    sqlcmd.Parameters.AddWithValue("@NZ_series_id", row[(int)ImportTemp1TableColumnName_Instock.NZ_series_id]);
                    sqlcmd.Parameters.AddWithValue("@AU_model_id", row[(int)ImportTemp1TableColumnName_Instock.AU_model_id]);
                    sqlcmd.Parameters.AddWithValue("@NZ_model_id", row[(int)ImportTemp1TableColumnName_Instock.NZ_model_id]);
                    sqlcmd.Parameters.AddWithValue("@BUID", row[(int)ImportTemp1TableColumnName_Instock.BUID]);
                    sqlcmd.Parameters.AddWithValue("@total_retail_price_au", row[(int)ImportTemp1TableColumnName_Instock.total_retail_price_au]);
                    sqlcmd.Parameters.AddWithValue("@total_retail_price_nz", row[(int)ImportTemp1TableColumnName_Instock.total_retail_price_nz]);               

                    sqlcmd.ExecuteNonQuery();

                    sqlcmd.Parameters.Clear();

                    recordInserted++;

                    Console.WriteLine("row id" + row[(int)ImportTemp1TableColumnName_Instock.row_id].ToString());
                    Console.WriteLine("recordInserted = " + recordInserted.ToString());

                }
            }
             catch (Exception ex)
             {
                 transaction.Rollback();
                 allInsertSuccessful = false;
                 throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ImportDataToSQLTemplateTable]") + ex.Message);
             }
             finally
             {
                 if (allInsertSuccessful == true && recordInserted > 0)
                 {
                     transaction.Commit();
                 }

                 sqlcmd.Dispose();
                 con.Close();
             }// end try

        } // end method


        /// <summary>
        /// Loop thru imported data from file and import each new model.
        /// </summary>
        public void ImportAndUpdateEachInstock(string filename, int processID, string databaseName)
        {

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);
            string returnMessage = string.Empty;

            try
            {
                string sql = "IMPORT_Instock_NewAndUpdate";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 0; 
                // add arguements
                sqlcmd.Parameters.AddWithValue("@filename", filename);
                sqlcmd.Parameters.AddWithValue("@processID", processID);
                sqlcmd.Parameters.AddWithValue("@ReturnMessage", returnMessage).Direction = ParameterDirection.Output;         
                sqlcmd.Parameters.AddWithValue("@DatabaseName", databaseName);


                sqlcmd.ExecuteNonQuery();

                Console.WriteLine(sqlcmd.Parameters["@ReturnMessage"].ToString());

                sqlcmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ImportAndUpdateEachInstock]") + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public List<String> GetMessageLogged()
        {
            List<String> msgList = new List<String>();

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            try
            {
                string sql = "IMPORT_GetImportMessageLogged";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // add arguements

                SqlDataReader myReader = null;
                myReader = sqlcmd.ExecuteReader();

                string message = string.Empty;

                while (myReader.Read())
                {
                    message = "[" + myReader["LoggedDate"].ToString() + "]";
                    message = message + "       " + "[" + myReader["MessageType"].ToString() + "]";
                    message = message + "       " + "[" + myReader["Message"].ToString() + "]";

                    msgList.Add(message);

                    // clear message
                    message = string.Empty;
                }

                sqlcmd.Parameters.Clear();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[LoadNextProcessID]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            return msgList;
        } // end method

        private DataTable GetErrorData()
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {
                con.Open();

                string sql = "IMPORT_Instock_GetErrorData";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetErrorData]") + ex.Message);
            }

            return table;

        } // end method

        public void ExportErrorDataToFile()
        {
            try
            {
                DataTable data = GetErrorData();

                if (data.Rows.Count != 0)
                {
                    string filePath = ConfigurationManager.AppSettings["FilePathErrorData"];
                    string datetime = string.Format("-{0:yyyyMMdd_hhmmss}", DateTime.Now);
                    string fileName = ConfigurationManager.AppSettings["InstockFilenameErrorData"] + datetime;
                    fileName += ConfigurationManager.AppSettings["InstockFileExtensionErrorData"];

                    RKLib.ExportData.Export export = new RKLib.ExportData.Export("Win");
                    export.ExportDetails(data, Export.ExportFormat.Excel,
                        filePath + fileName);

                }

            }
            catch (Exception ex)
            {

                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ExportErrorDataToFile]") + ex.Message);
            }
        }// end method

        private DataTable GetCompletedData()
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {
                con.Open();

                string sql = "IMPORT_Instock_GetCompletedData";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetErrorData]") + ex.Message);
            }

            return table;

        } // end method


        public void ExportCompletedDataToFile()
        {
            try
            {
                DataTable data = GetCompletedData();

                if (data.Rows.Count != 0)
                {
                    string filePath = ConfigurationManager.AppSettings["FilePathCompletedData"];
                    string datetime = string.Format("-{0:yyyyMMdd_hhmmss}", DateTime.Now);
                    string fileName = ConfigurationManager.AppSettings["InstockFilenameCompletedData"] + datetime;
                    fileName += ConfigurationManager.AppSettings["InstockFileExtensionCompletedData"];

                    RKLib.ExportData.Export export = new RKLib.ExportData.Export("Win");
                    export.ExportDetails(data, Export.ExportFormat.Excel,
                        filePath + fileName);
                }

            }
            catch (Exception ex)
            {

                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ExportCompletedDataToFile]") + ex.Message);
            }
        }// end method

        public void EmailImportComplete(string emailbody)
        {

            try
            {
                MailMessage message = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Host = ConfigurationManager.AppSettings["Instock_clientHOST"];

                message.From = new MailAddress(ConfigurationManager.AppSettings["Instock_emailAddressFrom"]);

                List<string> emailList = this.GetRecipientAddress();

                foreach (string address in emailList)
                {
                    message.To.Add(new MailAddress(address));
                }

                // Set the subject and message body text
                message.Subject = ConfigurationManager.AppSettings["Instock_emailsubject"];
                message.Body = emailbody;

                // Send the e-mail message
                client.Send(message);
            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[EmailImportComplete]") + ex.Message);
            }


        }

        private List<String> GetRecipientAddress()
        {
            List<String> emailList = new List<String>();

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            try
            {
                string sql = "Instock_GetEmailAddress";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // add arguements

                SqlDataReader myReader = null;
                myReader = sqlcmd.ExecuteReader();

                string emailAddress = string.Empty;

                while (myReader.Read())
                {
                    emailAddress = myReader["RecipientEmail"].ToString();
                    emailList.Add(emailAddress);

                }

                sqlcmd.Parameters.Clear();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetRecipientAddress]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            if (emailList.Count == 0)
            {
                throw new Exception("No recipient email address defined");
            }

            return emailList;
        } // end method

    }
}
