﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Common;

namespace DataAccessLayer
{
    public class DL_ExperlogixDatabase
    {

        #region variables

        private string className = "[DL_ExperlogixDatabase]";

        #endregion

        #region constructors

        public DL_ExperlogixDatabase()
        {

        }

        #endregion


        #region Public Methods

        /// <summary>
        /// Get a list of all Experlogix database name.
        /// </summary>
        /// <returns>Return a list of database name.</returns>
        public List<string> GetExperlogixDatabaseList()
        {
            
           List<string> list_database = new List<string>();
           SqlConnection con =  DL_Connection.CreateConnection();

           try
           {
               string sql = "DL_ExperlogixDatabase_GetExperlogixDatabaseList";

             
               SqlCommand sqlcmd = new SqlCommand(sql, con);
               // set to wait only for 10 sec else will confuse user why application did not start up
               // cannot override
               // sqlcmd.CommandTimeout = 2;
               con.Open();
               sqlcmd.CommandType = CommandType.StoredProcedure;

               SqlDataReader myReader = null;
               myReader = sqlcmd.ExecuteReader();

               while (myReader.Read())
               {               
                   list_database.Add(myReader["DatabaseName"].ToString());
               }

               sqlcmd.Parameters.Clear();
           }
           catch (SqlException ex)
           {
               throw new Exception(Common.Message.CustomExceptionMessage(this.className,"[GetExperlogixDatabase]") + ex.Message);

           }
           finally
           {
               con.Close();
           }

           return list_database; 
 
        }

        #endregion

        #region private methods


        #endregion
    }
}
