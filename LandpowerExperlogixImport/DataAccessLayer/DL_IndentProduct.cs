﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Common;
using System.Windows.Forms;
using System.Transactions;
using RKLib.ExportData;
using System.Net.Mail;


namespace DataAccessLayer
{
    public class DL_IndentProduct
    {
        private string className = "[DL_IndentProduct]";        

        public DL_IndentProduct()
        {

        }
        
        /// <summary>
        /// Clear Indent import tables
        /// </summary>
        public void ClearIndentSQLTemplateTable()
        {

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            try
            {
                string sql = "IMPORT_Indent_ResetTemplateIndent1";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.ExecuteNonQuery();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ClearIndentSQLTemplateTable]") + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public void ImportDataToSQLTemplateTable(DataGridView dv)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            string sql = "IMPORT_Indent_ImportGridDataToTempTable";

            SqlCommand sqlcmd = new SqlCommand(sql, con);
            con.Open();

            sqlcmd.CommandType = CommandType.StoredProcedure;

            SqlTransaction transaction;
            // Start a local transaction.
            transaction = con.BeginTransaction("InsertDataToTemp");
            sqlcmd.Transaction = transaction;
            bool allInsertSuccessful = true;
            int recordInserted = 0;

            try
            {

                Console.WriteLine("row count = " + dv.Rows.Count.ToString());

                foreach (DataGridViewRow row in dv.Rows)
                {                
                    // Last row is causing object reference issue
                    // +1 to advance to next record
                    if (recordInserted + 1 == dv.Rows.Count)
                    {
                        break;
                    }

                    sqlcmd.Parameters.AddWithValue("@Image", row.Cells[(int)ImportTemp1TableColumnName.Image].Value);
                    sqlcmd.Parameters.AddWithValue("@ProductCountryAvailability", row.Cells[(int)ImportTemp1TableColumnName.ProductCountryAvailability].Value);
                    sqlcmd.Parameters.AddWithValue("@SpecType", row.Cells[(int)ImportTemp1TableColumnName.SpecType].Value);
                    sqlcmd.Parameters.AddWithValue("@row_id", row.Cells[(int)ImportTemp1TableColumnName.row_id].Value);
                    sqlcmd.Parameters.AddWithValue("@serial_no", row.Cells[(int)ImportTemp1TableColumnName.serial_no].Value);
                    sqlcmd.Parameters.AddWithValue("@provisional_serial_no", row.Cells[(int)ImportTemp1TableColumnName.provisional_serial_no].Value);
                    sqlcmd.Parameters.AddWithValue("@entity", row.Cells[(int)ImportTemp1TableColumnName.entity].Value);
                    sqlcmd.Parameters.AddWithValue("@location", row.Cells[(int)ImportTemp1TableColumnName.location].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_stock_code", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_stock_code].Value);
                    sqlcmd.Parameters.AddWithValue("@mod_sup_stock_code", row.Cells[(int)ImportTemp1TableColumnName.mod_sup_stock_code].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_seq_no", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_seq_no].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_type", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_type].Value);
                    sqlcmd.Parameters.AddWithValue("@spec_stock_code", row.Cells[(int)ImportTemp1TableColumnName.spec_stock_code].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_description", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_description].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_standard_flag", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_standard_flag].Value);
                    sqlcmd.Parameters.AddWithValue("@spec_fitted_flag", row.Cells[(int)ImportTemp1TableColumnName.spec_fitted_flag].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_qty", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_qty].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_retail_price_aus", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_retail_price_aus].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_retail_price_nz", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_retail_price_nz].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_link", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_link].Value);
                    sqlcmd.Parameters.AddWithValue("@specs_sales_price_list", row.Cells[(int)ImportTemp1TableColumnName.specs_sales_price_list].Value);
                    sqlcmd.Parameters.AddWithValue("@mach_status", row.Cells[(int)ImportTemp1TableColumnName.mach_status].Value);
                    sqlcmd.Parameters.AddWithValue("@exp_arrival_date", row.Cells[(int)ImportTemp1TableColumnName.exp_arrival_date].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_group", row.Cells[(int)ImportTemp1TableColumnName.prod_group].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_group_desc", row.Cells[(int)ImportTemp1TableColumnName.prod_group_desc].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_class", row.Cells[(int)ImportTemp1TableColumnName.prod_class].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_class_desc", row.Cells[(int)ImportTemp1TableColumnName.prod_class_desc].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_cat", row.Cells[(int)ImportTemp1TableColumnName.prod_cat].Value);
                    sqlcmd.Parameters.AddWithValue("@prod_cat_desc", row.Cells[(int)ImportTemp1TableColumnName.prod_cat_desc].Value);
                    sqlcmd.Parameters.AddWithValue("@AU_series_id", row.Cells[(int)ImportTemp1TableColumnName.AU_series_id].Value);
                    sqlcmd.Parameters.AddWithValue("@NZ_series_id", row.Cells[(int)ImportTemp1TableColumnName.NZ_series_id].Value);
                    sqlcmd.Parameters.AddWithValue("@AU_model_id", row.Cells[(int)ImportTemp1TableColumnName.AU_model_id].Value);
                    sqlcmd.Parameters.AddWithValue("@NZ_model_id", row.Cells[(int)ImportTemp1TableColumnName.NZ_model_id].Value);


                    sqlcmd.ExecuteNonQuery();

                    sqlcmd.Parameters.Clear();

                    recordInserted++;                        

                    Console.WriteLine("row id" + row.Cells[(int)ImportTemp1TableColumnName.row_id].Value.ToString());
                    Console.WriteLine("recordInserted = " + recordInserted.ToString());

                    
                }

            }
            catch (Exception ex)
            {
                transaction.Rollback();
                allInsertSuccessful = false;
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ImportDataToSQLTemplateTable]") + ex.Message);
            }
            finally
            {
                if (allInsertSuccessful == true && recordInserted > 0)
                {
                    transaction.Commit();
                }

                sqlcmd.Dispose();
                con.Close();
            }// end try
        } // end method

        public int LoadNextProcessID()
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);
            int nextProcessID = 1;

            try
            {
                string sql = "IMPORT_GetNextProcessID";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // add arguements

                SqlDataReader myReader = null;
                myReader = sqlcmd.ExecuteReader();

                while (myReader.Read())
                {
                    nextProcessID = (int)myReader["NextProcessID"];
                }

                sqlcmd.Parameters.Clear();
      
            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[LoadNextProcessID]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            return nextProcessID;
        }

        /// <summary>
        /// Loop thru imported data from file and import each new model.
        /// </summary>
        public void ImportEachNewModel(string filename,int processID, string mode, string databaseName)
        {

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);
            string returnMessage = string.Empty;

            try
            {
                string sql = "IMPORT_Indent_Model";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 0;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@filename", filename);
                sqlcmd.Parameters.AddWithValue("@processID", processID);
                sqlcmd.Parameters.AddWithValue("@ReturnMessage", returnMessage).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.AddWithValue("@mode", mode);
                sqlcmd.Parameters.AddWithValue("@DatabaseName", databaseName);


                sqlcmd.ExecuteNonQuery();

                Console.WriteLine(sqlcmd.Parameters["@ReturnMessage"].Value.ToString());

                sqlcmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ImportEachNewModel]") + ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        public List<String> GetMessageLogged()
        {
            List<String> msgList = new List<String>();

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            try
            {
                string sql = "IMPORT_GetImportMessageLogged";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // add arguements

                SqlDataReader myReader = null;
                myReader = sqlcmd.ExecuteReader();

                string message = string.Empty;

                while (myReader.Read())
                {
                    message = "[" + myReader["LoggedDate"].ToString() + "]";
                    message = message + "       " + "[" + myReader["MessageType"].ToString() + "]";
                    message = message + "       " + "[" + myReader["Message"].ToString()  + "]";

                    msgList.Add(message);

                    // clear message
                    message = string.Empty;
                }

                sqlcmd.Parameters.Clear();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[LoadNextProcessID]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            return msgList;
        } // end method

        private DataTable GetErrorData()
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {           
                con.Open();

                string sql = "IMPORT_Indent_GetErrorData";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetErrorData]") + ex.Message);
            }

            return table;

        } // end method

        public void ExportErrorDataToFile()
        {
            try
            {
                DataTable data = GetErrorData();

                if (data.Rows.Count != 0)
                {
                    string filePath = ConfigurationManager.AppSettings["FilePathErrorData"];
                    string datetime = string.Format("-{0:yyyyMMdd_hhmmss}", DateTime.Now);
                    string fileName = ConfigurationManager.AppSettings["IndentFilenameErrorData"] + datetime;
                    fileName += ConfigurationManager.AppSettings["IndentFileExtensionErrorData"];

                    RKLib.ExportData.Export export = new RKLib.ExportData.Export("Win");
                    export.ExportDetails(data, Export.ExportFormat.Excel,
                        filePath + fileName);

                }

            }
            catch (Exception ex)
            {

                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ExportErrorDataToFile]") + ex.Message);
            }
        }// end method

        private DataTable GetCompletedData()
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {
                con.Open();

                string sql = "IMPORT_Indent_GetCompletedData";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetErrorData]") + ex.Message);
            }

            return table;

        } // end method

        public void ExportCompletedDataToFile()
        {
            try
            {
                DataTable data = GetCompletedData();

                if (data.Rows.Count != 0)
                {
                    string filePath = ConfigurationManager.AppSettings["FilePathCompletedData"];
                    string datetime = string.Format("-{0:yyyyMMdd_hhmmss}", DateTime.Now);
                    string fileName = ConfigurationManager.AppSettings["IndentFilenameCompletedData"] + datetime;
                    fileName += ConfigurationManager.AppSettings["IndentFileExtensionCompletedData"];

                    RKLib.ExportData.Export export = new RKLib.ExportData.Export("Win");
                    export.ExportDetails(data, Export.ExportFormat.Excel,
                        filePath + fileName);
                }

            }
            catch (Exception ex)
            {

                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[ExportCompletedDataToFile]") + ex.Message);
            }
        }// end method

        public bool LoadIsDatabaseSelectedIsValid(string selectedDatabaseName, string type)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            bool isDatabaseNameSelectedValid = false;

            try
            {
                string sql = "IMPORT_IsDatabaseSelectedValid";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@DatabaseName", selectedDatabaseName);
                sqlcmd.Parameters.AddWithValue("@Type", type);

                SqlDataReader myReader = null;
                myReader = sqlcmd.ExecuteReader();

                while (myReader.Read())
                {
                    isDatabaseNameSelectedValid = Convert.ToBoolean(myReader["IsValid"]);
                }

                sqlcmd.Parameters.Clear();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[LoadIsDatabaseSelectedIsValid]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            return isDatabaseNameSelectedValid;
        }

        public DataTable GetOptionCategoryAssignment(string optionID, string selectedDatabaseName)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {
                con.Open();

                string sql = "Indent_GetOptionCategoryAssignment";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 0;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@optionIDToSearch", optionID);
                sqlcmd.Parameters.AddWithValue("@DatabaseName", selectedDatabaseName);


                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetOptionCategoryAssignment]") + ex.Message);
            }

            return table;
        } // end method

        public DataTable GetOoptionModelAssignment(string optionID, string selectedDatabaseName)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {
                con.Open();

                string sql = "Indent_GetOptionModelAssignment";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 0;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@optionIDToSearch", optionID);
                sqlcmd.Parameters.AddWithValue("@DatabaseName", selectedDatabaseName);


                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetOoptionModelAssignment]") + ex.Message);
            }

            return table;
        } // end method

        public DataTable GetOoptionPrice(string optionID, string selectedDatabaseName)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {
                con.Open();

                string sql = "Indent_GetOptionPrice";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 0;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@optionIDToSearch", optionID);
                sqlcmd.Parameters.AddWithValue("@DatabaseName", selectedDatabaseName);

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetOoptionPrice]") + ex.Message);
            }

            return table;
        } // end method

        public DataTable GetOptionModelRuleAssignment(string optionID, string selectedDatabaseName)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {
                con.Open();

                string sql = "Indent_GetOptionModelRuleAssignment";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 0;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@optionIDToSearch", optionID);
                sqlcmd.Parameters.AddWithValue("@DatabaseName", selectedDatabaseName);

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetOptionModelRuleAssignment]") + ex.Message);
            }

            return table;
        } // end method


        public DataTable GetOptionProperty(string optionID, string selectedDatabaseName)
        {
            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            DataTable table = new DataTable();
            try
            {
                con.Open();

                string sql = "Indent_GetOptionProperty";

                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 0;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@optionIDToSearch", optionID);
                sqlcmd.Parameters.AddWithValue("@DatabaseName", selectedDatabaseName);

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlcmd;
                adapter.Fill(table);

            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetOptionProperty]") + ex.Message);
            }

            return table;
        } // end method


        public string DeleteOption(string optionID, string databaseName)
        {

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            int OptionNumDeleted = 0;
            int ModelOptionNumDeleted = 0;
            int OptionPropertyNumDeleted = 0;
            int OptionPriceNumDeleted = 0;
            string ErrorMessage = string.Empty;

            string returnMessage = string.Empty;

            try
            {
                string sql = "Indent_DeleteOption";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 0;
                // add arguements
                sqlcmd.Parameters.AddWithValue("@DatabaseName", databaseName);
                sqlcmd.Parameters.AddWithValue("@optionIDToSearch", optionID);
                sqlcmd.Parameters.AddWithValue("@OptionNumDeleted", OptionNumDeleted).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.AddWithValue("@ModelOptionNumDeleted", ModelOptionNumDeleted).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.AddWithValue("@OptionPropertyNumDeleted", OptionPropertyNumDeleted).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.AddWithValue("@OptionPriceNumDeleted", OptionPriceNumDeleted).Direction = ParameterDirection.Output;
                sqlcmd.Parameters.AddWithValue("@ErrorMessage", ErrorMessage).Direction = ParameterDirection.Output;

                sqlcmd.ExecuteNonQuery();

                ErrorMessage = sqlcmd.Parameters["@ErrorMessage"].Value.ToString();

                if (ErrorMessage == "")
                {

                    returnMessage = "Option Deleted = " + sqlcmd.Parameters["@OptionNumDeleted"].Value.ToString() + Environment.NewLine;
                    returnMessage += "Option Model Assignment Deleted = " + sqlcmd.Parameters["@ModelOptionNumDeleted"].Value.ToString() + Environment.NewLine;
                    returnMessage += "Option Property Deleted = " + sqlcmd.Parameters["@OptionPropertyNumDeleted"].Value.ToString() + Environment.NewLine;
                    returnMessage += "Option Price Deleted = " + sqlcmd.Parameters["@OptionPriceNumDeleted"].Value.ToString() + Environment.NewLine;
                }
                else
                {
                    returnMessage = ErrorMessage;
                }

                sqlcmd.Parameters.Clear();
            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[DeleteOption]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            return returnMessage;
        } // end method

        public void EmailImportComplete(string emailbody)
        {

            try
            {
                MailMessage message = new MailMessage();
                SmtpClient client = new SmtpClient();
                client.Host = ConfigurationManager.AppSettings["Indent_clientHOST"];

                message.From = new MailAddress(ConfigurationManager.AppSettings["Indent_emailAddressFrom"]);

                List<string> emailList = this.GetRecipientAddress();

                foreach (string address in emailList)
                {
                    message.To.Add(new MailAddress(address));
                }
               
                // Set the subject and message body text
                message.Subject = ConfigurationManager.AppSettings["Indent_emailsubject"];
                message.Body = emailbody;

                // Send the e-mail message
                client.Send(message);
            }
            catch (Exception ex)
            {
               throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[EmailImportComplete]") + ex.Message);
            }
        

        }

        private List<String> GetRecipientAddress()
        {
            List<String> emailList = new List<String>();

            // Connect to IMPORT database
            SqlConnection con = DL_Connection.CreateConnection(DL_Constant.AppConfigConnectionString);

            try
            {
                string sql = "Indent_GetEmailAddress";
                con.Open();
                SqlCommand sqlcmd = new SqlCommand(sql, con);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // add arguements

                SqlDataReader myReader = null;
                myReader = sqlcmd.ExecuteReader();

                string emailAddress = string.Empty;

                while (myReader.Read())
                {
                    emailAddress = myReader["RecipientEmail"].ToString();
                    emailList.Add(emailAddress);
                    
                }

                sqlcmd.Parameters.Clear();

            }
            catch (SqlException ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this.className, "[GetRecipientAddress]") + ex.Message);
            }
            finally
            {
                con.Close();
            }

            if (emailList.Count == 0) {
                throw new Exception("No recipient email address defined");
            }

            return emailList;
        } // end method

    }

}
