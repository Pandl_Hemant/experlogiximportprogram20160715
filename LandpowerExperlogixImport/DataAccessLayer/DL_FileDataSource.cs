﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data.Common;

namespace DataAccessLayer
{
    public class DL_FileDataSource
    {
        string _filename = string.Empty;
        string _filePath = string.Empty;
        string _className = "[DL_FileDataSource]";

        public DL_FileDataSource()
        {
            
        }

        public DL_FileDataSource(string filename,string filePath)
        {
            this._filename = filename;
            this._filePath = filePath;
        }

        public List<string> GetFileSheetNameList(string filePath)
        {
            List<string> sheets = new List<string>();

            DbConnection connection =  DL_Connection.CreateFileConnection(filePath);

            try
            {
                connection.Open();
                DataTable tbl = connection.GetSchema("Tables");
                connection.Close();

                foreach (DataRow row in tbl.Rows)
                {
                    string sheetName = (string)row["TABLE_NAME"];
                    // For sheet name to be valid, name needs to be start with letter and not numeric and file recognised as excel conform format.
                    // The sheet name cannot be too long either.
                    // NOTE: excel file exported by program has issue reading it as excel file. So to able to import file needs to be edit and save as excel file format.
                    if (sheetName.EndsWith("$"))
                    {
                        sheetName = sheetName.Substring(0, sheetName.Length - 1);
                        sheets.Add(sheetName);
                    }
                    // open up to accept any sheet
                    //sheetName = sheetName.Substring(0, sheetName.Length - 1);
                    //sheets.Add(sheetName);

                }
            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this._className, "[GetFileSheetNameList]") + ex.Message);
            }

            return sheets;

        }

        public DataTable GetFileSheetData(string sheetName, string filePath)
        {
            DataTable datatable = null;

            string connectionString = DL_Connection.CreateFileConnectionString(filePath);
            string query = String.Format("select * from [{0}$]", sheetName);

            try
            {
                OleDbDataAdapter dataAdapter = new OleDbDataAdapter(query, connectionString);

                DataSet dataSet = new DataSet();
                dataAdapter.Fill(dataSet);
                datatable = dataSet.Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(Common.Message.CustomExceptionMessage(this._className, "[GetFileSheetData]") + ex.Message);
            }

            return datatable;
        }

        public string Filename
        {
            get
            {
                return this._filename;
            }
            set
            {
                this._filename = value;
            }
        }

        public string FilePath
        {
            get
            {
                return this._filePath;
            }
            set
            {
                this._filePath = value;
            }
        }

    }
}
