﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    internal static class DL_Constant
    {

        internal readonly static string AppConfigConnectionString = "ConnectionString";
        internal readonly static string AppConfigConnectionStringIndent = "ConnectionString_Landpower";      

    }
}
