﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/* added */
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.OleDb;
using System.Data.Common;

namespace DataAccessLayer
{
    public class DL_Connection
    {
        /// <summary>
        /// Default connection to Import database.
        /// </summary>
        /// <returns></returns>
        public static SqlConnection CreateConnection()
        {
            return new SqlConnection(ConfigurationManager.AppSettings[DL_Constant.AppConfigConnectionString]);
        }

        /// <summary>
        /// Return connection with given appKey. 
        /// </summary>
        /// <param name="appKey"></param>
        /// <returns></returns>
        public static SqlConnection CreateConnection(string appKey)
        {
            return new SqlConnection(ConfigurationManager.AppSettings[appKey]);
        } 

        public static DbConnection CreateFileConnection(string filePath)
        {
            string connectionString = CreateFileConnectionString(filePath);
            DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.OleDb");
            DbConnection connection = factory.CreateConnection();
            connection.ConnectionString = connectionString;

            return connection;
        }

        public static string CreateFileConnectionString(string filePath)
        {
            string connectionStringDriver = ConfigurationManager.AppSettings["ExcelDriverVersion"];
            string connectionString = String.Format(connectionStringDriver, filePath);

            return connectionString;
        }
    }
}
