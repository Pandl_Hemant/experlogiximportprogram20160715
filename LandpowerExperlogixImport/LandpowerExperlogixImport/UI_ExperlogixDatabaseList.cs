﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/* Added */
using BusinessLogicLayer;

namespace LandpowerExperlogixImport
{
    public partial class UI_ExperlogixDatabaseList : Form
    {

        #region variables

        private string _selectedDatabase = string.Empty;

        private string _className = "[UI_ExperlogixDatabaseList_Load]";

        #endregion

        #region construtors

        public UI_ExperlogixDatabaseList()
        {
            InitializeComponent();

            /******************* added*************************/

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            // Set the MaximizeBox to false to remove the maximize box.
            this.MaximizeBox = false;
            // Set the MinimizeBox to false to remove the minimize box.
            this.MinimizeBox = false;
            // Set the start position of the form to the center of the screen.
            this.StartPosition = FormStartPosition.CenterScreen;
            // Display the form as a modal dialog box.

            // Define the border style of the form to a dialog box.
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
        }

        #endregion


        #region private methods

        private void UI_ExperlogixDatabaseList_Load(object sender, EventArgs e)
        {
            BL_ExperlogixDatabase database = new BL_ExperlogixDatabase();

            try
            {
                List<string> databaseList = database.GetExperlogixDatabaseList();

                this.comboBox_databaseList.DataSource = databaseList;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, this._className, MessageBoxButtons.OK, MessageBoxIcon.Error);
                // if connection issue get user to restart application for a retry.
                MessageBox.Show("Issue with database connection. Please restart your application to try again.", this._className, MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.DialogResult = System.Windows.Forms.DialogResult.Abort;
            }

        }


        private void button_OK_Click(object sender, EventArgs e)
        {
            if (comboBox_databaseList.SelectedIndex == -1)
            {

                // if sql connection okay but return no result then ask to see Administrator
                if (comboBox_databaseList.DataSource != null && comboBox_databaseList.Items.Count == 0)
                {
                    MessageBox.Show("No database available. Please contact your adminstrator for support.", this._className, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Please select a database from the list.", this._className, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
            else
            {
                this._selectedDatabase = comboBox_databaseList.SelectedValue.ToString();
                this.DialogResult = System.Windows.Forms.DialogResult.OK;    
            }

            
        }

        #endregion

        #region properties

        public string SelectedDatabase
        {
            get
            {
                return this._selectedDatabase;
            }
        }

        #endregion 

        private void button_Close_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Abort;
        }

       
    }
}
