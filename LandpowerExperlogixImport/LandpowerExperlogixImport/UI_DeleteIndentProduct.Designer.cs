﻿namespace LandpowerExperlogixImport
{
    partial class UI_DeleteIndentProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UI_DeleteIndentProduct));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_OptionID = new System.Windows.Forms.TextBox();
            this.button_Search = new System.Windows.Forms.Button();
            this.button_Delete = new System.Windows.Forms.Button();
            this.tabControl_categoryAssignment = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dataGridView_CategoryAssignment = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView_ModelAssignment = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView_OptionPrice = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView_ModelRuleAssignment = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.dataGridView_optionProperty = new System.Windows.Forms.DataGridView();
            this.groupBox_Input = new System.Windows.Forms.GroupBox();
            this.groupBox_output = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button_Close = new System.Windows.Forms.Button();
            this.tabControl_categoryAssignment.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_CategoryAssignment)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ModelAssignment)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_OptionPrice)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ModelRuleAssignment)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_optionProperty)).BeginInit();
            this.groupBox_Input.SuspendLayout();
            this.groupBox_output.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(147, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "OptionID(supplier stock code)";
            // 
            // textBox_OptionID
            // 
            this.textBox_OptionID.Location = new System.Drawing.Point(160, 21);
            this.textBox_OptionID.Name = "textBox_OptionID";
            this.textBox_OptionID.Size = new System.Drawing.Size(359, 20);
            this.textBox_OptionID.TabIndex = 1;
            // 
            // button_Search
            // 
            this.button_Search.Location = new System.Drawing.Point(536, 19);
            this.button_Search.Name = "button_Search";
            this.button_Search.Size = new System.Drawing.Size(75, 23);
            this.button_Search.TabIndex = 2;
            this.button_Search.Text = "Search";
            this.button_Search.UseVisualStyleBackColor = true;
            this.button_Search.Click += new System.EventHandler(this.button_Search_Click);
            // 
            // button_Delete
            // 
            this.button_Delete.Location = new System.Drawing.Point(625, 19);
            this.button_Delete.Name = "button_Delete";
            this.button_Delete.Size = new System.Drawing.Size(75, 23);
            this.button_Delete.TabIndex = 3;
            this.button_Delete.Text = "Delete";
            this.button_Delete.UseVisualStyleBackColor = true;
            this.button_Delete.Click += new System.EventHandler(this.button_Delete_Click);
            // 
            // tabControl_categoryAssignment
            // 
            this.tabControl_categoryAssignment.Controls.Add(this.tabPage1);
            this.tabControl_categoryAssignment.Controls.Add(this.tabPage2);
            this.tabControl_categoryAssignment.Controls.Add(this.tabPage3);
            this.tabControl_categoryAssignment.Controls.Add(this.tabPage4);
            this.tabControl_categoryAssignment.Controls.Add(this.tabPage5);
            this.tabControl_categoryAssignment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_categoryAssignment.Location = new System.Drawing.Point(3, 16);
            this.tabControl_categoryAssignment.Name = "tabControl_categoryAssignment";
            this.tabControl_categoryAssignment.SelectedIndex = 0;
            this.tabControl_categoryAssignment.Size = new System.Drawing.Size(812, 387);
            this.tabControl_categoryAssignment.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dataGridView_CategoryAssignment);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(804, 361);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Category Assignment";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dataGridView_CategoryAssignment
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_CategoryAssignment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_CategoryAssignment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_CategoryAssignment.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView_CategoryAssignment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_CategoryAssignment.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_CategoryAssignment.Name = "dataGridView_CategoryAssignment";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_CategoryAssignment.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView_CategoryAssignment.Size = new System.Drawing.Size(798, 355);
            this.dataGridView_CategoryAssignment.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView_ModelAssignment);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(804, 361);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Model Assignment";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView_ModelAssignment
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_ModelAssignment.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView_ModelAssignment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView_ModelAssignment.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView_ModelAssignment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_ModelAssignment.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_ModelAssignment.Name = "dataGridView_ModelAssignment";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_ModelAssignment.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView_ModelAssignment.Size = new System.Drawing.Size(798, 355);
            this.dataGridView_ModelAssignment.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView_OptionPrice);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(804, 361);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Option Price";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView_OptionPrice
            // 
            this.dataGridView_OptionPrice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_OptionPrice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_OptionPrice.Location = new System.Drawing.Point(0, 0);
            this.dataGridView_OptionPrice.Name = "dataGridView_OptionPrice";
            this.dataGridView_OptionPrice.Size = new System.Drawing.Size(804, 361);
            this.dataGridView_OptionPrice.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView_ModelRuleAssignment);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(804, 361);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Model Rule Assignment";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView_ModelRuleAssignment
            // 
            this.dataGridView_ModelRuleAssignment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_ModelRuleAssignment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_ModelRuleAssignment.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_ModelRuleAssignment.Name = "dataGridView_ModelRuleAssignment";
            this.dataGridView_ModelRuleAssignment.Size = new System.Drawing.Size(798, 355);
            this.dataGridView_ModelRuleAssignment.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.dataGridView_optionProperty);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(804, 361);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Option Property";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // dataGridView_optionProperty
            // 
            this.dataGridView_optionProperty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_optionProperty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_optionProperty.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_optionProperty.Name = "dataGridView_optionProperty";
            this.dataGridView_optionProperty.Size = new System.Drawing.Size(798, 355);
            this.dataGridView_optionProperty.TabIndex = 0;
            // 
            // groupBox_Input
            // 
            this.groupBox_Input.Controls.Add(this.button_Close);
            this.groupBox_Input.Controls.Add(this.label2);
            this.groupBox_Input.Controls.Add(this.button_Search);
            this.groupBox_Input.Controls.Add(this.label1);
            this.groupBox_Input.Controls.Add(this.button_Delete);
            this.groupBox_Input.Controls.Add(this.textBox_OptionID);
            this.groupBox_Input.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox_Input.Location = new System.Drawing.Point(0, 0);
            this.groupBox_Input.Name = "groupBox_Input";
            this.groupBox_Input.Size = new System.Drawing.Size(818, 61);
            this.groupBox_Input.TabIndex = 5;
            this.groupBox_Input.TabStop = false;
            // 
            // groupBox_output
            // 
            this.groupBox_output.Controls.Add(this.tabControl_categoryAssignment);
            this.groupBox_output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_output.Location = new System.Drawing.Point(0, 61);
            this.groupBox_output.Name = "groupBox_output";
            this.groupBox_output.Size = new System.Drawing.Size(818, 406);
            this.groupBox_output.TabIndex = 6;
            this.groupBox_output.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "(E.g. A21-310-PG_N_%)";
            // 
            // button_Close
            // 
            this.button_Close.Location = new System.Drawing.Point(715, 19);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(75, 23);
            this.button_Close.TabIndex = 5;
            this.button_Close.Text = "Close";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // UI_DeleteIndentProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 467);
            this.Controls.Add(this.groupBox_output);
            this.Controls.Add(this.groupBox_Input);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "UI_DeleteIndentProduct";
            this.Text = "Delete Indent Product";
            this.tabControl_categoryAssignment.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_CategoryAssignment)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ModelAssignment)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_OptionPrice)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_ModelRuleAssignment)).EndInit();
            this.tabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_optionProperty)).EndInit();
            this.groupBox_Input.ResumeLayout(false);
            this.groupBox_Input.PerformLayout();
            this.groupBox_output.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_OptionID;
        private System.Windows.Forms.Button button_Search;
        private System.Windows.Forms.Button button_Delete;
        private System.Windows.Forms.TabControl tabControl_categoryAssignment;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.DataGridView dataGridView_CategoryAssignment;
        private System.Windows.Forms.DataGridView dataGridView_ModelAssignment;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.DataGridView dataGridView_OptionPrice;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataGridView_ModelRuleAssignment;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.DataGridView dataGridView_optionProperty;
        private System.Windows.Forms.GroupBox groupBox_Input;
        private System.Windows.Forms.GroupBox groupBox_output;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button_Close;
    }
}