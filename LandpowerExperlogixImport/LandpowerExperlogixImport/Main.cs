﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/* Added */
using BusinessLogicLayer;
using System.Threading;
using System.Configuration;

namespace LandpowerExperlogixImport
{
    public partial class UI_Main : Form
    {


        #region variables

        string _className = "[UI_Main]";
        string _selectedDatabase = string.Empty;
        string _statusDatabase = "Database Selected: ";
        string _fileName = string.Empty;
        string _filePath = string.Empty;
        BL_FileDataSource _bl_fileDataSource = null;
        BL_IndentProduct _bl_indentProduct = null;
        BL_Instock _bl_instock = null;

        // make sure no more than one thread running
        bool _isProcessRunning = false;

        #endregion 

        #region constructors

        public UI_Main()
        {
            InitializeComponent();

        }
        #endregion


        #region methods

        private void UI_Main_Load(object sender, EventArgs e)
        {
           
            UI_ExperlogixDatabaseList databaselist = new UI_ExperlogixDatabaseList();

            if (databaselist.ShowDialog() == DialogResult.OK)
            {

                this._selectedDatabase = databaselist.SelectedDatabase;

                this.StatusLabel.Text = this._statusDatabase + this._selectedDatabase;

            }          
            else
            {
                databaselist.Close();
                // close main form
                this.Close();
                Application.Exit();

            }

            // initialise object
            this._bl_indentProduct = new BL_IndentProduct(this._selectedDatabase);
            this._bl_instock = new BL_Instock(this._selectedDatabase);
        }

        #endregion


        private void button_browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            DialogResult dlgResult = dlg.ShowDialog();
            if (dlgResult == DialogResult.OK)
            {
                this.textBox_FileLocation.Text = dlg.FileName;
                this._filePath = this.textBox_FileLocation.Text;
                this._fileName = System.IO.Path.GetFileName(this.textBox_FileLocation.Text);

                this._bl_indentProduct.ImportFileName = this._fileName;
                this._bl_instock.ImportFilename = this._fileName;

                this.textBox_output.AppendText("File selected: " + this._fileName + Environment.NewLine);

                try
                {
                    if (this._bl_fileDataSource == null)
                    {
                        _bl_fileDataSource = new BL_FileDataSource(this._fileName, this._filePath);
                    }
                    else
                    {
                        _bl_fileDataSource.FileName = this._fileName;
                        _bl_fileDataSource.FilePath = this._filePath;
                    }

                    this.comboBox_LoadData.DataSource = _bl_fileDataSource.GetFileSheetNameList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to read excel sheetname. " + ex.Message,this._className +  "Reading Excel sheet name",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearForm();
                }
            }
        }

        private void clearForm()
        {
            this.textBox_FileLocation.Text = string.Empty;
            this.comboBox_LoadData.DataSource = null;            
            this.dataGridView_data.DataSource = null;
        }

        private void button_loadData_Click(object sender, EventArgs e)
        {

            if (this._isProcessRunning)
            {
                return;
            }

            string sheetName = string.Empty;

            if (this.comboBox_LoadData.SelectedIndex != -1)
            {
                sheetName = this.comboBox_LoadData.SelectedItem.ToString();
            }
            else
            {
                MessageBox.Show("Please select excel file then select a sheet name to load data.", this._className + "Load data", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataTable dt = null;

            if (System.IO.File.Exists(_bl_fileDataSource.FilePath))
            {
                try
                {
                    UI_ProgressBar progressBar = new UI_ProgressBar();
                    progressBar.SetIndeterminate(true);

                    Thread backgroundThread = new Thread(
                        new ThreadStart(() =>
                    {
                        try
                        {
                            this._isProcessRunning = true;
                            dt = this._bl_fileDataSource.GetFileSheetData(sheetName);
                            progressBar.BeginInvoke(new Action(() => progressBar.Close()));
                            this._isProcessRunning = false;
                        }
                        catch (Exception ex1)
                        {
                            MessageBox.Show("Failed to load data to screen[btn_LoadData_Click]. " + ex1.Message, this._className + "Loading data to screen",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);

                            // close progress bar if error
                            progressBar.BeginInvoke(new Action(() => progressBar.Close()));
                            // reset so process can start again. Note once the thread reached this end of open and close brackets it will end.  
                            this._isProcessRunning = false;
                        }
                    }
                    ));



                    backgroundThread.IsBackground = true;
                    backgroundThread.Start();
                    
                    progressBar.ShowDialog();

                    this.dataGridView_data.DataSource = dt;
                    DisableDataGridColumnsAllowSorting();
                    this.textBox_output.AppendText("Data Load completed: " + Environment.NewLine);

                    // kill thread if it still alive
                    if (backgroundThread.IsAlive)
                    {
                        backgroundThread.Abort();
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show("Failed to load data to screen[btn_LoadData_Click]. " + ex.Message,this._className + "Loading data to screen",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("No File is Selected");
            }
        }

        /// <summary>
        /// Disable datagrid columns sorting.
        /// </summary>
        private void DisableDataGridColumnsAllowSorting()
        {
            if (this.dataGridView_data.DataSource != null)
            {
                for (int i = 0; i < dataGridView_data.Columns.Count - 1; i++)
                {
                    dataGridView_data.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
        }

        private void IndentNew_ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (this._isProcessRunning)
            {
                return;
            }

            // Make sure selected database is a valid for import
            if (!this.Indent_DatabaseSelectedIsValid())
            {
                return;
            }           
           

            try
            {
                UI_ProgressBar progressBar = new UI_ProgressBar();
                progressBar.SetIndeterminate(true);

                Thread backgroundThread = new Thread(
                       new ThreadStart(() =>
                       {
                           
                           try
                           {
                               this._isProcessRunning = true;
                               this._bl_indentProduct.ImportNewModel(this.dataGridView_data, "NEW", this._selectedDatabase);
                               this.BeginInvoke(new Action(() => ProcessOutput_Indent()));
                               progressBar.BeginInvoke(new Action(() => progressBar.Close()));
                               this._isProcessRunning = false;
                           }
                           catch (Exception ex1)
                           {
                               MessageBox.Show("File format issue." + ex1.Message,this._className + " Import new indent", MessageBoxButtons.OK, MessageBoxIcon.Error);
                               // close progress bar if error
                               progressBar.BeginInvoke(new Action(() => progressBar.Close()));
                               // reset so process can start again. Note once the thread reached this end of open and close brackets it will end.  
                               this._isProcessRunning = false;
                                   
                           }
                           
                       }
                 ));

                backgroundThread.IsBackground = true;
                backgroundThread.Start();

                progressBar.ShowDialog();

                MessageBox.Show("Import Complete","Import New Indent",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error importing Indent machine" + ex.Message, this._className + " Import new indent", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void IndentUpdate_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this._isProcessRunning)
            {
                return;
            }

            // Make sure selected database is a valid for import
            if (!this.Indent_DatabaseSelectedIsValid())
            {
                return;
            }      

            try
            {
                UI_ProgressBar progressBar = new UI_ProgressBar();
                progressBar.SetIndeterminate(true);

                Thread backgroundThread = new Thread(
                       new ThreadStart(() =>
                       {
                           try
                           {
                               this._isProcessRunning = true;
                               this._bl_indentProduct.ImportNewModel(this.dataGridView_data, "UPDATE", this._selectedDatabase);
                               this.BeginInvoke(new Action(() => ProcessOutput_Indent()));                               
                               progressBar.BeginInvoke(new Action(() => progressBar.Close()));
                               this._isProcessRunning = false;
                           }
                           catch (Exception ex1)
                           {
                               MessageBox.Show("File format issue." + ex1.Message, this._className + " Update indent", MessageBoxButtons.OK, MessageBoxIcon.Error);
                               // close progress bar if error
                               progressBar.BeginInvoke(new Action(() => progressBar.Close()));
                               // reset so process can start again. Note once the thread reached this end of open and close brackets it will end.  
                               this._isProcessRunning = false;
                           }
                       }
                 ));

                backgroundThread.IsBackground = true;
                backgroundThread.Start();

                progressBar.ShowDialog();

                MessageBox.Show("Update Complete", "Import New Indent", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error updating indent machine" + ex.Message, this._className + " Update indent", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        } // end method

        private void InstockNewUpdate_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this._isProcessRunning)
            {
                return;
            }

            // Make sure selected database is a valid for import
            if (!this.Instock_DatabaseSelectedIsValid())
            {
                return;
            }

            try
            {
                UI_ProgressBar progressBar = new UI_ProgressBar();
                progressBar.SetIndeterminate(true);

                Thread backgroundThread = new Thread(
                       new ThreadStart(() =>
                       {
                           try
                           {
                               this._isProcessRunning = true;
                               this._bl_instock.ImportNewAndUpdateInstock(this.dataGridView_data, this._selectedDatabase);
                               this.BeginInvoke(new Action(() => ProcessOutput_Instock()));
                               progressBar.BeginInvoke(new Action(() => progressBar.Close()));
                               this._isProcessRunning = false;
                           }
                           catch (Exception ex1)
                           {
                               MessageBox.Show("File format issue." + ex1.Message, this._className + " Import & Update Instock ", MessageBoxButtons.OK, MessageBoxIcon.Error);
                               // close progress bar if error
                               progressBar.BeginInvoke(new Action(() => progressBar.Close()));
                               // reset so process can start again. Note once the thread reached this end of open and close brackets it will end.  
                               this._isProcessRunning = false;
                           }
                       }
                 ));

                backgroundThread.IsBackground = true;
                backgroundThread.Start();

                progressBar.ShowDialog();

                MessageBox.Show("Import & Update Complete", "Import Instock", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error importing & updating instock machine" + ex.Message, this._className + " Import & Update instock", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void ProcessOutput_Indent()
        {
            // output to winform log tab
            outputToOutputTab_Indent();
            // output file to Error folder
            outputErrorDataToFile_Indent();
            // output file to Complete folder
            outputCompletedDataToFile_Indent();

        }

        private void ProcessOutput_Instock()
        {
            // output to winform log tab
            outputToOutputTab_Instock();
            // output file to Error folder
            outputErrorDataToFile_Instock();
            // output file to Complete folder
            outputCompletedDataToFile_Instock();

        }

        private void outputToOutputTab_Indent()
        {
            string emailbody = string.Empty;

            List<String> messageList = this._bl_indentProduct.GetMessageLogged();

            foreach (string msg in messageList)
            {
                this.textBox_output.AppendText(msg + Environment.NewLine);
                emailbody += msg + Environment.NewLine;
            }


            if (ConfigurationManager.AppSettings["Indent_EmailOn"] == "True")
            {

                try
                {
                    // send email
                    this._bl_indentProduct.EmailImportComplete(emailbody);
                    
                    this.textBox_output.AppendText("Indent Email sent" + Environment.NewLine);
                }
                catch (Exception ex)
                {
                    this.textBox_output.AppendText(ex.Message + Environment.NewLine);
                }
            }


        }

        private void outputToOutputTab_Instock()
        {
            string emailbody = string.Empty;

            List<String> messageList = this._bl_instock.GetMessageLogged();

            foreach (string msg in messageList)
            {
                this.textBox_output.AppendText(msg + Environment.NewLine);
                emailbody += msg + Environment.NewLine;
            }

            if (ConfigurationManager.AppSettings["Instock_EmailOn"] == "True")
            {

                try
                {
                    // send email
                    this._bl_instock.EmailImportComplete(emailbody);

                    this.textBox_output.AppendText("Instock Email sent" + Environment.NewLine);
                }
                catch (Exception ex)
                {
                    this.textBox_output.AppendText(ex.Message + Environment.NewLine);
                }
            }
        }

        private void outputErrorDataToFile_Indent()
        {
            this._bl_indentProduct.ExportErrorDataToFile();
            this.textBox_output.AppendText("Complete ExportErrorDataToFile " + Environment.NewLine);
        }

        private void outputErrorDataToFile_Instock()
        {
            this._bl_instock.ExportErrorDataToFile();
            this.textBox_output.AppendText("Complete ExportErrorDataToFile " + Environment.NewLine);
        }

        private void outputCompletedDataToFile_Indent()
        {
            this._bl_indentProduct.ExportCompletedDataToFile();
            this.textBox_output.AppendText("Complete ExportProcessedDataToFile " + Environment.NewLine);
        }

        private void outputCompletedDataToFile_Instock()
        {
            this._bl_instock.ExportCompletedDataToFile();
            this.textBox_output.AppendText("Complete ExportProcessedDataToFile " + Environment.NewLine);
        }

        private void changeDatabaseToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            UI_ExperlogixDatabaseList databaselist = new UI_ExperlogixDatabaseList();

            if (databaselist.ShowDialog() == DialogResult.OK)
            {

                this._selectedDatabase = databaselist.SelectedDatabase;

                this.StatusLabel.Text = this._statusDatabase + this._selectedDatabase;

                // update database name change
                this._bl_indentProduct.SelectedDatabaseName = this._selectedDatabase;
                this._bl_instock.SelectedDatabaseName = this._selectedDatabase;

            }      
        }

        private bool Indent_DatabaseSelectedIsValid()
        {
            if (!this._bl_indentProduct.IsSelectedDatabaseNameValid)
            {
                string message = "Database " + this._selectedDatabase + " selected is not valid. Please select a correct one.";
                MessageBox.Show(message, "Database Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
            }

            return true;
        }

        private bool Instock_DatabaseSelectedIsValid()
        {
            if (!this._bl_instock.IsSelectedDatabaseNameValid)
            {
                string message = "Database " + this._selectedDatabase + " selected is not valid. Please select a correct one.";
                MessageBox.Show(message, "Database Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return false;
            }

            return true;
        }

        private void deleteModelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Make sure selected database is a valid for import
            if (!this.Indent_DatabaseSelectedIsValid())
            {
                return;
            }      

            UI_DeleteIndentProduct frmDeleteIndent = new UI_DeleteIndentProduct(this._bl_indentProduct);

            frmDeleteIndent.ShowDialog();
        }

       
    }
}
