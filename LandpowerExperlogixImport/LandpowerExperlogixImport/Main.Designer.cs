﻿namespace LandpowerExperlogixImport
{
    partial class UI_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UI_Main));
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.indentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IndentNew_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.IndentUpdate_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InstockNewUpdate_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.changeDatabaseToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_ImportFile = new System.Windows.Forms.GroupBox();
            this.button_loadData = new System.Windows.Forms.Button();
            this.button_browse = new System.Windows.Forms.Button();
            this.comboBox_LoadData = new System.Windows.Forms.ComboBox();
            this.textBox_FileLocation = new System.Windows.Forms.TextBox();
            this.label_sheetName = new System.Windows.Forms.Label();
            this.label_fileLocation = new System.Windows.Forms.Label();
            this.tabControl_Main = new System.Windows.Forms.TabControl();
            this.tabPage_Data = new System.Windows.Forms.TabPage();
            this.dataGridView_data = new System.Windows.Forms.DataGridView();
            this.tabPage_log = new System.Windows.Forms.TabPage();
            this.textBox_output = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.statusStrip.SuspendLayout();
            this.menuStrip.SuspendLayout();
            this.groupBox_ImportFile.SuspendLayout();
            this.tabControl_Main.SuspendLayout();
            this.tabPage_Data.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_data)).BeginInit();
            this.tabPage_log.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 445);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(818, 22);
            this.statusStrip.TabIndex = 0;
            this.statusStrip.Text = "Import Database:";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(53, 17);
            this.StatusLabel.Text = "Database";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indentToolStripMenuItem,
            this.instockToolStripMenuItem,
            this.changeDatabaseToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(818, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "menuStrip1";
            // 
            // indentToolStripMenuItem
            // 
            this.indentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.IndentNew_ToolStripMenuItem,
            this.IndentUpdate_ToolStripMenuItem,
            this.deleteModelToolStripMenuItem});
            this.indentToolStripMenuItem.Name = "indentToolStripMenuItem";
            this.indentToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.indentToolStripMenuItem.Text = "Indent";
            // 
            // IndentNew_ToolStripMenuItem
            // 
            this.IndentNew_ToolStripMenuItem.Name = "IndentNew_ToolStripMenuItem";
            this.IndentNew_ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.IndentNew_ToolStripMenuItem.Text = "Import New Model";
            this.IndentNew_ToolStripMenuItem.Click += new System.EventHandler(this.IndentNew_ToolStripMenuItem_Click);
            // 
            // IndentUpdate_ToolStripMenuItem
            // 
            this.IndentUpdate_ToolStripMenuItem.Name = "IndentUpdate_ToolStripMenuItem";
            this.IndentUpdate_ToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.IndentUpdate_ToolStripMenuItem.Text = "Update Existing Model";
            this.IndentUpdate_ToolStripMenuItem.Click += new System.EventHandler(this.IndentUpdate_ToolStripMenuItem_Click);
            // 
            // deleteModelToolStripMenuItem
            // 
            this.deleteModelToolStripMenuItem.Name = "deleteModelToolStripMenuItem";
            this.deleteModelToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.deleteModelToolStripMenuItem.Text = "Delete Existing Model";
            this.deleteModelToolStripMenuItem.Click += new System.EventHandler(this.deleteModelToolStripMenuItem_Click);
            // 
            // instockToolStripMenuItem
            // 
            this.instockToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.InstockNewUpdate_ToolStripMenuItem});
            this.instockToolStripMenuItem.Name = "instockToolStripMenuItem";
            this.instockToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.instockToolStripMenuItem.Text = "Instock";
            // 
            // InstockNewUpdate_ToolStripMenuItem
            // 
            this.InstockNewUpdate_ToolStripMenuItem.Name = "InstockNewUpdate_ToolStripMenuItem";
            this.InstockNewUpdate_ToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.InstockNewUpdate_ToolStripMenuItem.Text = "Import New and Update";
            this.InstockNewUpdate_ToolStripMenuItem.Click += new System.EventHandler(this.InstockNewUpdate_ToolStripMenuItem_Click);
            // 
            // changeDatabaseToolStripMenuItem
            // 
            this.changeDatabaseToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.changeDatabaseToolStripMenuItem1});
            this.changeDatabaseToolStripMenuItem.Name = "changeDatabaseToolStripMenuItem";
            this.changeDatabaseToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.changeDatabaseToolStripMenuItem.Text = "Database";
            // 
            // changeDatabaseToolStripMenuItem1
            // 
            this.changeDatabaseToolStripMenuItem1.Name = "changeDatabaseToolStripMenuItem1";
            this.changeDatabaseToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.changeDatabaseToolStripMenuItem1.Text = "Select Database";
            this.changeDatabaseToolStripMenuItem1.Click += new System.EventHandler(this.changeDatabaseToolStripMenuItem1_Click);
            // 
            // groupBox_ImportFile
            // 
            this.groupBox_ImportFile.Controls.Add(this.button_loadData);
            this.groupBox_ImportFile.Controls.Add(this.button_browse);
            this.groupBox_ImportFile.Controls.Add(this.comboBox_LoadData);
            this.groupBox_ImportFile.Controls.Add(this.textBox_FileLocation);
            this.groupBox_ImportFile.Controls.Add(this.label_sheetName);
            this.groupBox_ImportFile.Controls.Add(this.label_fileLocation);
            this.groupBox_ImportFile.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox_ImportFile.Location = new System.Drawing.Point(0, 24);
            this.groupBox_ImportFile.Name = "groupBox_ImportFile";
            this.groupBox_ImportFile.Size = new System.Drawing.Size(818, 71);
            this.groupBox_ImportFile.TabIndex = 2;
            this.groupBox_ImportFile.TabStop = false;
            // 
            // button_loadData
            // 
            this.button_loadData.Location = new System.Drawing.Point(701, 37);
            this.button_loadData.Name = "button_loadData";
            this.button_loadData.Size = new System.Drawing.Size(91, 23);
            this.button_loadData.TabIndex = 5;
            this.button_loadData.Text = "Load Data";
            this.button_loadData.UseVisualStyleBackColor = true;
            this.button_loadData.Click += new System.EventHandler(this.button_loadData_Click);
            // 
            // button_browse
            // 
            this.button_browse.Location = new System.Drawing.Point(701, 11);
            this.button_browse.Name = "button_browse";
            this.button_browse.Size = new System.Drawing.Size(91, 23);
            this.button_browse.TabIndex = 4;
            this.button_browse.Text = "Browse...";
            this.button_browse.UseVisualStyleBackColor = true;
            this.button_browse.Click += new System.EventHandler(this.button_browse_Click);
            // 
            // comboBox_LoadData
            // 
            this.comboBox_LoadData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_LoadData.FormattingEnabled = true;
            this.comboBox_LoadData.Location = new System.Drawing.Point(114, 37);
            this.comboBox_LoadData.Name = "comboBox_LoadData";
            this.comboBox_LoadData.Size = new System.Drawing.Size(581, 21);
            this.comboBox_LoadData.TabIndex = 3;
            // 
            // textBox_FileLocation
            // 
            this.textBox_FileLocation.Location = new System.Drawing.Point(114, 13);
            this.textBox_FileLocation.Name = "textBox_FileLocation";
            this.textBox_FileLocation.ReadOnly = true;
            this.textBox_FileLocation.Size = new System.Drawing.Size(581, 20);
            this.textBox_FileLocation.TabIndex = 2;
            // 
            // label_sheetName
            // 
            this.label_sheetName.AutoSize = true;
            this.label_sheetName.Location = new System.Drawing.Point(42, 40);
            this.label_sheetName.Name = "label_sheetName";
            this.label_sheetName.Size = new System.Drawing.Size(66, 13);
            this.label_sheetName.TabIndex = 1;
            this.label_sheetName.Text = "Sheet Name";
            // 
            // label_fileLocation
            // 
            this.label_fileLocation.AutoSize = true;
            this.label_fileLocation.Location = new System.Drawing.Point(12, 16);
            this.label_fileLocation.Name = "label_fileLocation";
            this.label_fileLocation.Size = new System.Drawing.Size(96, 13);
            this.label_fileLocation.TabIndex = 0;
            this.label_fileLocation.Text = "Excel File Location";
            // 
            // tabControl_Main
            // 
            this.tabControl_Main.Controls.Add(this.tabPage_Data);
            this.tabControl_Main.Controls.Add(this.tabPage_log);
            this.tabControl_Main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_Main.Location = new System.Drawing.Point(3, 16);
            this.tabControl_Main.Name = "tabControl_Main";
            this.tabControl_Main.SelectedIndex = 0;
            this.tabControl_Main.Size = new System.Drawing.Size(812, 331);
            this.tabControl_Main.TabIndex = 3;
            // 
            // tabPage_Data
            // 
            this.tabPage_Data.Controls.Add(this.dataGridView_data);
            this.tabPage_Data.Location = new System.Drawing.Point(4, 22);
            this.tabPage_Data.Name = "tabPage_Data";
            this.tabPage_Data.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_Data.Size = new System.Drawing.Size(804, 305);
            this.tabPage_Data.TabIndex = 0;
            this.tabPage_Data.Text = "Data";
            this.tabPage_Data.UseVisualStyleBackColor = true;
            // 
            // dataGridView_data
            // 
            this.dataGridView_data.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_data.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView_data.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_data.Name = "dataGridView_data";
            this.dataGridView_data.Size = new System.Drawing.Size(798, 299);
            this.dataGridView_data.TabIndex = 0;
            // 
            // tabPage_log
            // 
            this.tabPage_log.Controls.Add(this.textBox_output);
            this.tabPage_log.Location = new System.Drawing.Point(4, 22);
            this.tabPage_log.Name = "tabPage_log";
            this.tabPage_log.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_log.Size = new System.Drawing.Size(804, 305);
            this.tabPage_log.TabIndex = 1;
            this.tabPage_log.Text = "Output";
            this.tabPage_log.UseVisualStyleBackColor = true;
            // 
            // textBox_output
            // 
            this.textBox_output.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_output.Location = new System.Drawing.Point(3, 3);
            this.textBox_output.Multiline = true;
            this.textBox_output.Name = "textBox_output";
            this.textBox_output.ReadOnly = true;
            this.textBox_output.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox_output.Size = new System.Drawing.Size(798, 299);
            this.textBox_output.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tabControl_Main);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(818, 350);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // UI_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(818, 467);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox_ImportFile);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip;
            this.Name = "UI_Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "iDeal Experlogix Data Import";
            this.Load += new System.EventHandler(this.UI_Main_Load);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.groupBox_ImportFile.ResumeLayout(false);
            this.groupBox_ImportFile.PerformLayout();
            this.tabControl_Main.ResumeLayout(false);
            this.tabPage_Data.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_data)).EndInit();
            this.tabPage_log.ResumeLayout(false);
            this.tabPage_log.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem indentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem IndentNew_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem IndentUpdate_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteModelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instockToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox_ImportFile;
        private System.Windows.Forms.Button button_loadData;
        private System.Windows.Forms.Button button_browse;
        private System.Windows.Forms.ComboBox comboBox_LoadData;
        private System.Windows.Forms.TextBox textBox_FileLocation;
        private System.Windows.Forms.Label label_sheetName;
        private System.Windows.Forms.Label label_fileLocation;
        private System.Windows.Forms.TabControl tabControl_Main;
        private System.Windows.Forms.TabPage tabPage_Data;
        private System.Windows.Forms.TabPage tabPage_log;
        private System.Windows.Forms.TextBox textBox_output;
        private System.Windows.Forms.DataGridView dataGridView_data;
        private System.Windows.Forms.ToolStripMenuItem changeDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem changeDatabaseToolStripMenuItem1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ToolStripMenuItem InstockNewUpdate_ToolStripMenuItem;

    }
}

