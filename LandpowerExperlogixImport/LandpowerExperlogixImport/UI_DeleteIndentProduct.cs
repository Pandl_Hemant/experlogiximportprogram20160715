﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/* Added */
using BusinessLogicLayer;
using System.Threading;


namespace LandpowerExperlogixImport
{
    public partial class UI_DeleteIndentProduct : Form
    {
        string _className = "[UI_DeleteIndentProduct]";
        BL_IndentProduct _indentProduct = null;
       

        public UI_DeleteIndentProduct()
        {
            InitializeComponent();
        }

        public UI_DeleteIndentProduct(BL_IndentProduct indentProduct)
        {
            InitializeComponent();

            this._indentProduct = indentProduct;
        }


        private void LoadSearchData(string optionID)
        {
            try
            {
                DataTable categoryAssignment = this._indentProduct.GetOptionCategoryAssignment(optionID);

                this.dataGridView_CategoryAssignment.DataSource = categoryAssignment;

                DataTable modelAssignment = this._indentProduct.GetOptionModelAssignment(optionID);
                this.dataGridView_ModelAssignment.DataSource = modelAssignment;

                DataTable price = this._indentProduct.GetOptionPrice(optionID);
                this.dataGridView_OptionPrice.DataSource = price;

                DataTable modelRule = this._indentProduct.GetOptionModelRuleAssignment(optionID);
                this.dataGridView_ModelRuleAssignment.DataSource = modelRule;

                DataTable optionProperty = this._indentProduct.GetOptionProperty(optionID);
                this.dataGridView_optionProperty.DataSource = optionProperty;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error load search data " + ex.Message, this._className + "LoadSearchData", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void button_Search_Click(object sender, EventArgs e)
        {
            this.LoadSearchData(this.textBox_OptionID.Text);           
        }

        private void button_Delete_Click(object sender, EventArgs e)
        {
            string warningMessage = "IMPORTANT PLEASE READ FIRST" + Environment.NewLine;
                warningMessage += "DO NOT DELETE any data if model has already been published and quoted by users." + Environment.NewLine;
                warningMessage += "Only use delete function when data was imported into system incorrectly and the model has not been used on a quote." + Environment.NewLine;
                warningMessage += Environment.NewLine;
                warningMessage += "This is irreversible!" + Environment.NewLine;
                warningMessage += "Are you sure you want to delete option like " + this.textBox_OptionID.Text;

                if (MessageBox.Show(warningMessage, "Delete Confirmation", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)
                {
                    string returnMessage = string.Empty;

                    try
                    {
                        returnMessage = this._indentProduct.DeleteOption(this.textBox_OptionID.Text);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error delete data " + ex.Message, this._className + "button_Delete_Click", MessageBoxButtons.OK, MessageBoxIcon.Error);

                    }

                    MessageBox.Show(returnMessage, "Option Deleted");
                    // refresh data on screen
                    this.LoadSearchData(this.textBox_OptionID.Text);
                }            
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }



    }
}
