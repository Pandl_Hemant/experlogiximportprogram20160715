﻿#define DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

 
namespace WS_Instock
{
     

    static class Program
    {
       

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

           /* uncomment for test 
            #if DEBUG 

            iDealInstockImport myservice = new iDealInstockImport();
            myservice.Test();
            
            
            #else*/
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new iDealInstockImport() 
            };
            ServiceBase.Run(ServicesToRun);
            /*#endif*/

        }
    }
}
