﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
/* Added */
using System.IO;
using BusinessLogicLayer;
using System.Configuration;
using System.Globalization;
using System.Timers;


namespace WS_Instock
{
    public partial class iDealInstockImport : ServiceBase
    {

        BL_Instock _bl_instock = null;
        string _logFileDirectoryAndName = string.Empty;
        private Timer _timer;
        string _currentMonth = string.Empty;
        // whether instock still importing
        bool _isProcessing = false;

        public iDealInstockImport()
        {
            InitializeComponent();
        }

        public void Test()
        {
            OnStart(null);
            //OnStop();
        }

        private void SetTimer()
        {
            if (_timer == null)
            {
                _timer = new Timer();
                _timer.AutoReset = true;
                _timer.Interval = 60000 * Convert.ToDouble(ConfigurationManager.AppSettings["IntervalMinutes"]);
                _timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
                _timer.Start();
            }
        }

        private void timer_Elapsed(object source, System.Timers.ElapsedEventArgs e)
        {
            // change filename each month
            if(CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month) != this._currentMonth)
            {
                this.LoadFileNameAndOutputDirectory();
            }

            try
            {
                this.writeToFile("timer_Elapsed started");
                this.writeToFile(this._isProcessing.ToString());
                Process();
                this.writeToFile("timer_Elapsed finished");
                this.writeToFile(this._isProcessing.ToString());

            }
            catch (Exception ex)
            {
                string message = "Exception[timer_Elapsed]: " + ex.Message;
                writeToFile(message);
            }

        }

        private void LoadFileNameAndOutputDirectory()
        {
            string logFileOutputDirectory = ConfigurationManager.AppSettings["LogFileOutputDirectory"];
            // work out the file name
            string year = DateTime.Now.Year.ToString();
            _currentMonth = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);

            string fileName = "InstockImportLog_" + year + _currentMonth + ".txt";

            this._logFileDirectoryAndName = logFileOutputDirectory + fileName;
            this.writeToFile("LoadFileNameAndOutputDirectory");
        }

        protected override void OnStart(string[] args)
        {

            try
            {

                this.LoadFileNameAndOutputDirectory();

                this.writeToFile("Windows Service Start");    
            
                
                // load instock object
                if (this._bl_instock == null)
                {
                    // load database name 
                    // if databse name change in config file service needs to be restarted. 
                    string datbaseName = ConfigurationManager.AppSettings["DatabaseName"];
                    this._bl_instock = new BL_Instock(datbaseName);
                }
            
                // ensure database name selected is valid for instock
                if (!this._bl_instock.IsSelectedDatabaseNameValid)
                {
                    string message = "Database " + this._bl_instock.SelectedDatabaseName + " defined in Config file is not a valid Instock datbase.";
                    this.writeToFile(message);

                    // set this to true so timer will not process if service has not been stopped by service controller
                    this._isProcessing = true;
                    this.Stop();                
                    //this.Wai.WaitForStatus(ServiceControllerStatus.Stopped);
                    return;
                }

                Process();     
               
                // initialise timer
                this.SetTimer();

            }
            catch (Exception ex)
            {
                string message = "Exception[OnStart]: " + ex.Message;
                writeToFile(message);
            }
        }

        protected override void OnStop()
        {
            writeToFile("Windows Service Stop");  
        }

        private string[] GetFileList()
        {
            string[] files = null;
            try
            {
                files = Directory.GetFiles(ConfigurationManager.AppSettings["InstockFileDirectory"], ConfigurationManager.AppSettings["InstockFileSearchPattern"]);
            }
            catch (Exception ex)
            {
                string message = "Exception[GetFileList] " + ex.Message;
                writeToFile(message);  
            }
            return files;
        }

        private void Process()
        {

            if (this._isProcessing)
            {
                // if process still importing wait...
                return;
            }

            this._isProcessing = true;

            // Get import file to process
            string[] files = GetFileList();
            string fileName = string.Empty;

            for (int i = 0; i < files.Length; i++)
            {
                try
                {
                    Console.WriteLine(files[i]);
                    string filePath = files[i];
                    fileName = System.IO.Path.GetFileName(filePath);

                    this.writeToFile("Start importing file " + fileName);

                    BL_FileDataSource fileDataSource = new BL_FileDataSource(fileName, filePath);

                    List<string> sheetList = fileDataSource.GetFileSheetNameList();

                    // Assumption here is data is always on the first sheet.
                    DataTable data = fileDataSource.GetFileSheetData(sheetList[0]);

                    // set filename
                    this._bl_instock.ImportFilename = fileName;

                    this._bl_instock.ImportNewAndUpdateInstock(data, this._bl_instock.SelectedDatabaseName);

                    // output to fiel and email
                    this.WriteResultToFileAndEmail();
                    // output file to Error folder
                    outputErrorDataToFile_Instock();
                    // output file to Complete folder
                    outputCompletedDataToFile_Instock();
                    // move processed file to processed folder
                    this.MoveProcessedFile(fileName);

                    this.writeToFile("Finish importing file " + fileName);
                }
                catch (Exception ex)
                {
                    string message = "Exception[Process]: " + fileName + ". " + ex.Message;
                    this.writeToFile(message);
                    this.MoveProcessedFile(fileName);
                    // email to alert users to check file and reprocess
                    string emailbody = "Exception[Process]: " + "Issue with processing file " + fileName + " please investigate and reprocess the file."; 
                    this._bl_instock.EmailImportComplete(emailbody);
                }

                
            }

            this._isProcessing = false;
        }//  end method

        private void writeToFile(string message)
        {
            // if file exists append. If file does not exists create the file.
            // The using statement automatically closes the stream and calls  
            // IDisposable.Dispose on the stream object. 
            using (StreamWriter writer = new System.IO.StreamWriter(this._logFileDirectoryAndName, true))
            {
                writer.WriteLine(DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "   " + message);
            }
        }

        private void WriteResultToFileAndEmail()
        {
            string emailbody = string.Empty;

            List<String> messageList = this._bl_instock.GetMessageLogged();

            foreach (string msg in messageList)
            {
                this.writeToFile(msg);                
                emailbody += msg + Environment.NewLine;
            }

            if (ConfigurationManager.AppSettings["Instock_EmailOn"] == "True")
            {
                try
                {
                    // send email
                    this._bl_instock.EmailImportComplete(emailbody);

                    this.writeToFile("Instock Email sent");
                    
                }
                catch (Exception ex)
                {
                    string message = "Exception[WriteResultToFileAndEmail]: " + ex.Message;
                    this.writeToFile(message);                    
                }
            }

        } // end method

        private void outputErrorDataToFile_Instock()
        {
            this._bl_instock.ExportErrorDataToFile();
            this.writeToFile("Complete ExportErrorDataToFile");
            
        }

        private void outputCompletedDataToFile_Instock()
        {
            this._bl_instock.ExportCompletedDataToFile();
            this.writeToFile("Complete ExportProcessedDataToFile");            
        }

        private void MoveProcessedFile(string fileName)
        {
            // source file
            string instockFileDirectory = ConfigurationManager.AppSettings["InstockFileDirectory"] + fileName;
            // destination 
            string filePathProcessedFile = ConfigurationManager.AppSettings["FilePathProcessedFile"] + fileName;
            // move file
            Directory.Move(instockFileDirectory, filePathProcessedFile);
            
            string message = "file " + fileName + " has moved to " + filePathProcessedFile;

            this.writeToFile(message);

        }
    }
}
