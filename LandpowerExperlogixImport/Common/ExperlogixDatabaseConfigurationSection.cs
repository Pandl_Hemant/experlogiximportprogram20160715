﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// added
using System.Configuration;

namespace Common
{
    public class ExperlogixDatabaseConfigurationSection : ConfigurationSection
    {
        [ConfigurationProperty("products")]
        public DatabaseElementCollection Products
        {
            get { return (DatabaseElementCollection)this["products"]; }
        }
    }

    public class DatabaseElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return this["name"].ToString(); }
        }

        [ConfigurationProperty("ConnectionString")]
        public string Price
        {
            get { return this["ConnectionString"].ToString(); }
        }
    }

    public class DatabaseElementCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new DatabaseElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DatabaseElement)element).Name;
        }
    }


}
