﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{

    public enum ImportTemp1TableColumnName
    {
        Image = 0,
        ProductCountryAvailability = 1,
        SpecType = 2,
        row_id = 3,
        serial_no = 4,
        provisional_serial_no = 5,
        entity = 6,
        location = 7,
        specs_sales_stock_code = 8,
        mod_sup_stock_code = 9,
        specs_sales_seq_no = 10,
        specs_sales_type = 11,
        spec_stock_code = 12,
        specs_sales_description = 13,
        specs_sales_standard_flag = 14,
        spec_fitted_flag = 15,
        specs_sales_qty = 16,
        specs_sales_retail_price_aus = 17,
        specs_sales_retail_price_nz = 18,
        specs_sales_link = 19,
        specs_sales_price_list = 20,
        mach_status = 21,
        exp_arrival_date = 22,
        prod_group = 23,
        prod_group_desc = 24,
        prod_class = 25,
        prod_class_desc = 26,
        prod_cat = 27,
        prod_cat_desc = 28,
        AU_series_id = 29,
        NZ_series_id = 30,
        AU_model_id = 31,
        NZ_model_id = 32
    }

    public enum ImportTemp1TableColumnName_Instock
    {
        Image = 0,
        ProductCountryAvailability = 1,
        SpecType = 2,
        row_id = 3,
        serial_no = 4,
        provisional_serial_no = 5,
        entity = 6,
        location = 7,
        specs_sales_stock_code = 8,
        mod_sup_stock_code = 9,
        specs_sales_seq_no = 10,
        specs_sales_type = 11,
        spec_stock_code = 12,
        specs_sales_description = 13,
        specs_sales_standard_flag = 14,
        spec_fitted_flag = 15,
        specs_sales_qty = 16,
        specs_sales_retail_price_aus = 17,
        specs_sales_retail_price_nz = 18,
        specs_sales_link = 19,
        specs_sales_price_list = 20,
        mach_status = 21,
        exp_arrival_date = 22,
        prod_group = 23,
        prod_group_desc = 24,
        prod_class = 25,
        prod_class_desc = 26,
        prod_cat = 27,
        prod_cat_desc = 28,
        mach_total_cost = 29, // added
        instock_country = 30, // added 
        AU_series_id = 31,
        NZ_series_id = 32,
        AU_model_id = 33,
        NZ_model_id = 34,
        BUID = 35, // added
        total_retail_price_au = 36, // added
        total_retail_price_nz = 37 // added
    }
}
